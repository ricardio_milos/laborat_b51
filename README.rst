::

   ██▓        ▄▄▄          ▄▄▄▄       ▒█████            ██▀███      ▄▄▄         ▄▄▄█████▓
  ▓██▒       ▒████▄       ▓█████▄    ▒██▒  ██▒         ▓██ ▒ ██▒   ▒████▄       ▓  ██▒ ▓▒
  ▒██░       ▒██  ▀█▄     ▒██▒ ▄██   ▒██░  ██▒         ▓██ ░▄█ ▒   ▒██  ▀█▄     ▒ ▓██░ ▒░
  ▒██░       ░██▄▄▄▄██    ▒██░█▀     ▒██   ██░         ▒██▀▀█▄     ░██▄▄▄▄██    ░ ▓██▓ ░
  ░██████▒    ▓█   ▓██▒   ░▓█  ▀█▓   ░ ████▓▒░         ░██▓ ▒██▒    ▓█   ▓██▒     ▒██▒ ░
  ░ ▒░▓  ░    ▒▒   ▓▒█░   ░▒▓███▀▒   ░ ▒░▒░▒░          ░ ▒▓ ░▒▓░    ▒▒   ▓▒█░     ▒ ░░
  ░ ░ ▒  ░     ▒   ▒▒ ░   ▒░▒   ░      ░ ▒ ▒░            ░▒ ░ ▒░     ▒   ▒▒ ░       ░
    ░ ░        ░   ▒       ░    ░    ░ ░ ░ ▒             ░░   ░      ░   ▒        ░
      ░  ░         ░  ░    ░             ░ ░              ░              ░  ░
                                ░

********************************************************************

**Auteure et Auteurs**

*Miriam Davydov* - *Louis Barbonet* - *Patrick Quevillion* - *Karl Boulais*

********************************************************************

Présentation du projet
***********************

********************************************************************

Division du travail
####################
    *    Nous avons fait quatre rencontres d’équipe durant notre projet:
        * Pour la première rencontre nous avons mis sur pied le plan de travail et la division des tâches.
        * Pour la seconde, nous avons révisé et discuté des progrès ainsi que des difficultés rencontrées.
        * Pour la troisième, nous avons révisé les progrès et discuter des quelques autres tactiques pour résoudre les problèmes.
        * Pour la quatrième, nous avons révisé l’entièreté du travail, particulièrement les détails de la mise en forme, ajout de derniers points.

********************************************************************

Séparation des responsabilités
##############################
* Karl pour le plan UML, la présentation du projet et l'organisation des rencontres et code divers
* Louis pour affichage fenêtre et algorithme de déplacement
* Miriam pour les tests de fonctionnalités et code
* Patrick pour les algorithmes déplacement et la mise en forme du code

********************************************************************

Structure du projet
####################
* Multifichiers
* Objet orienté

********************************************************************

Structure spécifique du code
#############################
* laborat_app.py
    * Classes
        * LaboratApp
            * S'occupe des états de l'application
            * S'occupe de l'appel de tous les fonctions qui définissent Laborat
        
* app_enum.py
    * Classes
        * AppState
            * Définit les états possibles de la classe LaboratApp
        * MovementDirection
            * Définit les différents déplacements possibles

* key_handler.py
    * Classes
        * KeyHandler
            * S’occupe du comportement des touches en général
        * PlayerHandler
            * S’occupe des mouvements du joueur et dépendamment de la touche utilisée faire bouger l’entité souris
        * ExitHandler
            * S'occupe de l'arrêt de l'application et les touches responsable de son arrêt
        * RestartHandler
            * S'occupe du redémarrage de la partie

* entities.py
    * Classes
        * Fromage 
            * Définition de l'entité fromage
        * RatZombie
            * S'occupe des états des rats zombies
        * Mur
            * Définition de l'entité Mur
        * Joueur
            * S'occupe des états du joueur
        * Plancher
            * s'occupe de définir les états du plancher

* visual_entity.py
    * Classes
        * Entity
            * Responsable de toutes les entités du jeu
        * VisualEntitity
            * Responsable de l’affichage du jeu (environnement et contenu)
        * MoveableEntity
            * Responsable des déplacements des entités sur la grille du jeu

* map_info.py
    * Classes
        * MapInfo
            * S'occupe de génerer la map sur la grille du jeu en y plaçant les entités

* path_finding.py
    * Classes
        * Node
            * Responsable des Nodes utilisées dans le PathFinding
        * PathFinding
            * Responsable de la façon dont les rats zombies se dirigent vers le joueur

* graphics.py
    * Classes
        * Graphic
            * Met à jour l'affichage des graphiques
            * importe le chemin et charges les images
