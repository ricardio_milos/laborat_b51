#!/usr/bin/env python

import pygame
import map_info
import key_handler
from app_enum import AppState
from visual_entity import VisualEntity, MoveableEntity
from graphics import Graphic

class LaboratApp:
    _instance = None

    @staticmethod
    def getInstance():
        """ Static access method. """
        if LaboratApp._instance is None:
            LaboratApp()
        return LaboratApp._instance

    def __init__(self):
        """ Virtually private constructor. """
        if LaboratApp._instance is not None:
            raise Exception("There cannot be two LaboratApp instances !")
        else:
            LaboratApp._instance = self
            self._backgroundColor = [0, 0, 0]
            self._screen = None
            self._mapInfo = map_info.MapInfo()
            self._gestionnaires_touche = []
            self._fps = 60
            self._tps = 4
            self.__tpsRate = 1 / self._tps * 1000
            self._state = AppState.Stopped
            self._clock = pygame.time.Clock()
            pygame.init()

    def __del__(self):
        pygame.quit()

    def requestQuit(self):
        self.setState(AppState.Quit)

    def requestRestart(self):
        self.setState(AppState.Stopped)

    def setState(self, state):
        self._state = state

        if state is not AppState.Running:
            for e in self._mapInfo.get_entities_by_type(MoveableEntity):
                e.is_movement_possible = False

    def initGame(self):
        self._gestionnaires_touche.clear()
        self._gestionnaires_touche.append(key_handler.ExitHandler(self))
        self._gestionnaires_touche.append(key_handler.RestartHandler(self))
        self._mapInfo.generate(self, (16, 10), (64, 64))

    def runGame(self):
        self.initGame()
        self._screen = pygame.display.set_mode(self._mapInfo.screen_size)
        self._state = AppState.Running

        elapsed_time = 0.0
        current_tick = 1
        max_tick_between_graphic_logic = self._fps / self._tps
        stop_state = [AppState.Quit, AppState.Stopped]
        while self._state not in stop_state:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.requestQuit()

            key = pygame.key.get_pressed()

            # Gestion des touches
            key_handler.KeyHandler.verifier_touche_pressee(key)

            # Only update entities at self.__tpsRate, ex.: 200ms
            if elapsed_time > self.__tpsRate:
                # Update entities
                self._mapInfo.update()
                elapsed_time = 0
                current_tick = 1

            # Redessine le background
            self._screen.fill(self._backgroundColor)
            
            # Update sprite for animation
            Graphic.updateGraphics(current_tick, max_tick_between_graphic_logic)

            # Dessin des VisualEntity
            VisualEntity.drawEntities(self._screen, self._mapInfo.tile_size)

            if self._state == AppState.Victory:
                self.displayWin()
            elif self._state == AppState.Defeat:
                self.displayLose()
            
            pygame.display.update()

            # Framerate
            elapsed_time += self._clock.tick(self._fps)
            current_tick += 1

        if self._state is AppState.Running:
            self._state = AppState.Stopped

    def executable(self):
        while self._state != AppState.Quit:
            self.runGame()

    def drawFPS(self):
        font = pygame.font.SysFont("Arial", 18)
        fps_text = font.render(str(int(self._clock.get_fps())), 1, pygame.Color("white"))
        self._screen.blit(fps_text, (985,10))

    def displayWin(self):
        font = pygame.font.SysFont("Arial", 40)
        win_text = font.render('La partie est gagné :D', 1, pygame.Color("white"))
        text_rect = win_text.get_rect(center=(self._mapInfo.screen_size[0]/2, self._mapInfo.screen_size[1]/2))
        self._screen.blit(win_text, text_rect)

    def displayLose(self):
        font = pygame.font.SysFont("Arial", 40)
        lose_text = font.render('La partie est perdu :(', 1, pygame.Color("red"))
        text_rect = lose_text.get_rect(center=(self._mapInfo.screen_size[0]/2, self._mapInfo.screen_size[1]/2))
        self._screen.blit(lose_text, text_rect)