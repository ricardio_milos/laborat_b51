#!/usr/bin/env python

import pygame

from graphics import Graphic
from app_enum import MovementDirection
class Entity:
    _entities = []

    @staticmethod
    def updateEntities(mapInfo):
        for entity in Entity._entities:
            entity.update(mapInfo)

    def __init__(self, position):
        self.position = position
        self._observers = set()
        Entity._entities.append(self)

    def addObserver(self, observer):
        self._observers.add(observer)

    def _notifyObservers(self):
        for observer in self._observers:
            observer.notify(self)

    def update(self, mapInfo):
        pass

    def _offsetPosition(self, offset):
        self._setPosition((self.position[0] + offset[0], self.position[1] + offset[1]))

    def _setPosition(self, position):
        self.position = position


class VisualEntity(Entity, Graphic):
    _visual_entities = []

    @staticmethod
    def drawEntities(screen, tile_size):
        for entity in VisualEntity._visual_entities:
            entity.draw(screen, tile_size)

    @staticmethod
    def delete_entities():
        VisualEntity._visual_entities.clear()

    def __init__(self, position, lien_image, is_collision_possible = True, display_priority = 0):
        Entity.__init__(self, position)
        Graphic.__init__(self, lien_image, priority=display_priority)
        self._is_collision_possible = is_collision_possible
        VisualEntity._visual_entities.append(self)

    def onCollision(self, entity):
        pass

    def update(self, mapInfo):
        pass

    def draw(self, screen, tile_size):
        if self._current_sprite < len(self._images):
            rect = pygame.Rect((self.position[0] * tile_size[0], self.position[1] * tile_size[1]), tile_size)
            image = pygame.transform.scale(self._images[self._current_sprite], tile_size)
            screen.blit(image, rect)

class MoveableEntity(VisualEntity):
    @staticmethod
    def calculate_movement(position, *movements):
        new_position = position
        for movement in movements:
            new_position = (new_position[0] + movement[0], new_position[1] + movement[1])
        
        return new_position

    def __init__(self, position, base_image_path, is_movement_possible = True, is_collision_possible = True):
        super().__init__(position, None, is_collision_possible)
        self.direction = MovementDirection.NoDirection
        self._base_image_path = base_image_path
        self.update_image_path()

        self._was_moved = False
        self.previous_position = position
        self.is_movement_possible = is_movement_possible

    def update_image_path(self):
        self.set_image_path(self._base_image_path + self.direction.name)

    def update(self, mapInfo):
        pass

    def _move(self, mapInfo, position, is_offset=False):
        if is_offset:
            position = MoveableEntity.calculate_movement(self.position, position)

        if self.is_movement_possible and self.is_movement_allowed(position, mapInfo):
            # offset[0] = offsetX, offset[1] = offsetY
            self._setPosition(position)
            if not self.verifyCollision():
                self._notifyObservers()
                return True
        return False

    def is_movement_allowed(self, new_position, mapInfo):
        return mapInfo.is_position_inside_map(new_position)

    def verifyCollision(self):
        if self._is_collision_possible and self._was_moved:
            for entity in VisualEntity._visual_entities:
                if entity._is_collision_possible and entity.position == self.position and entity is not self:
                    self.onCollision(entity)
                    entity.onCollision(self)
                    self._setPosition(self.previous_position)
                    self._was_moved = False
                    return True
        return False

    def _setPosition(self, position):
        self.previous_position = self.position
        self.position = position
        self._was_moved = True