import entities
import visual_entity
import pygame
from path_finding import PathFinding

class MapInfo:
    def __init__(self):
        self.player = None
        self.player_distance_grid = {}
        self.entities = []
        self.entities_by_tile = {}
        self.map_size = (0, 0)
        self.map = None
        self.tile_size = (0, 0)
        self.screen_size = (0, 0)

    def generate(self, app, map_size, tile_size):
        self.map_size = map_size
        self.map = pygame.Rect((0, 0), self.map_size)
        self.tile_size = tile_size
        self.screen_size = (map_size[0] * tile_size[0], map_size[1] * tile_size[1])
        self.entities.clear()
        self.entities_by_tile.clear()
        visual_entity.VisualEntity.delete_entities()
        

        # Create floor
        for x in range(0, self.map_size[0]):
            for y in range(0, self.map_size[1]):
                entities.Plancher((x, y))

        # Create a map and place entities
        self.player = entities.Joueur((0, 0), app)
        self.entities.append(self.player)
        self.entities.append(entities.Fromage((15, 9)))
        self.entities.append(entities.RatZombie((15, 0)))
        self.entities.append(entities.RatZombie((0, 9)))
        self.entities.append(entities.RatZombie((10, 9)))

        self.entities.append(entities.Mur((6, 0)))
        self.entities.append(entities.Mur((9, 0)))

        self.entities.append(entities.Mur((1, 1)))
        self.entities.append(entities.Mur((2, 1)))
        self.entities.append(entities.Mur((4, 1)))

        self.entities.append(entities.Mur((6, 2)))
        self.entities.append(entities.Mur((7, 2)))
        self.entities.append(entities.Mur((11, 2)))
        self.entities.append(entities.Mur((14, 2)))
        self.entities.append(entities.Mur((15, 2)))

        self.entities.append(entities.Mur((1, 3)))
        self.entities.append(entities.Mur((3, 3)))
        self.entities.append(entities.Mur((4, 3)))
        self.entities.append(entities.Mur((10, 3)))
        self.entities.append(entities.Mur((11, 3)))
        
        self.entities.append(entities.Mur((5, 4)))
        self.entities.append(entities.Mur((9, 4)))
        self.entities.append(entities.Mur((13, 4)))

        self.entities.append(entities.Mur((0, 5)))
        self.entities.append(entities.Mur((2, 5)))
        self.entities.append(entities.Mur((8, 5)))
        self.entities.append(entities.Mur((13, 5)))
        self.entities.append(entities.Mur((15, 5)))

        self.entities.append(entities.Mur((5, 6)))
        self.entities.append(entities.Mur((8, 6)))
        self.entities.append(entities.Mur((10, 6)))
        self.entities.append(entities.Mur((11, 6)))
        self.entities.append(entities.Mur((12, 6)))

        self.entities.append(entities.Mur((1, 7)))
        self.entities.append(entities.Mur((2, 7)))
        self.entities.append(entities.Mur((5, 7)))
        self.entities.append(entities.Mur((6, 7)))
        self.entities.append(entities.Mur((7, 7)))
        self.entities.append(entities.Mur((8, 7)))
        self.entities.append(entities.Mur((11, 7)))
        self.entities.append(entities.Mur((14, 7)))

        self.entities.append(entities.Mur((1, 8)))
        self.entities.append(entities.Mur((8, 8)))
        self.entities.append(entities.Mur((9, 8)))
        self.entities.append(entities.Mur((11, 8)))
        self.entities.append(entities.Mur((13, 8)))
        self.entities.append(entities.Mur((14, 8)))

        self.entities.append(entities.Mur((3, 9)))
        self.entities.append(entities.Mur((6, 9)))
        self.entities.append(entities.Mur((11, 9)))

        self._registerEntities()
        self.generate_player_distance_grid()

    def is_position_inside_map(self, position):
        return self.map.contains(pygame.Rect(position, (1,1)))

    def _registerEntities(self):
        for entity in self.entities:
            self.entities_by_tile[entity.position] = entity
            entity.addObserver(self)

    def notify(self, entity):
        if isinstance(entity, visual_entity.MoveableEntity):
            self.entities_by_tile.pop(entity.previous_position)
            self.entities_by_tile[entity.position] = entity
        if entity is self.player:
            self.generate_player_distance_grid()

    def generate_player_distance_grid(self):
        self.player_distance_grid = PathFinding.generate_distance_grid(self, self.player.position)

    def update(self):
        visual_entity.Entity.updateEntities(self)

    def get_entity_from_position(self, position):
        return self.entities_by_tile.get(position, None)

    def get_entities_by_type(self, entity_type):
        matching_entities = []
        for e in self.entities:
            if isinstance(e, entity_type):
                matching_entities.append(e)

        return matching_entities