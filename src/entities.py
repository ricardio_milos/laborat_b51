#!/usr/bin/env python
import os

from visual_entity import VisualEntity, MoveableEntity
from key_handler import PlayerHandler
from app_enum import AppState, MovementDirection
from path_finding import PathFinding

def getPath(folder, assetName):
    """ Get absolute path for assets """
    execPath = os.path.dirname(__file__)
    assetsPath = os.path.join(execPath, folder)
    assetPath = os.path.join(assetsPath, assetName)
    return assetPath

class Joueur(MoveableEntity):
    def __init__(self, position, app):
        super().__init__(position, getPath('assets', 'Laborat-'), True)
        self._handler = PlayerHandler(self)
        self._app = app
        

    def update(self, mapInfo):
        if self.direction != MovementDirection.NoDirection:
            self._move(mapInfo, self.direction.value, is_offset=True)
            

    def onCollision(self, entity):
        if isinstance(entity, Fromage):
            self._app.setState(AppState.Victory)
        elif isinstance(entity, RatZombie):
            self._app.setState(AppState.Defeat)


class Fromage(VisualEntity):
    def __init__(self, position):
        super().__init__(position, getPath('assets', 'fromage'))


class RatZombie(MoveableEntity):
    def __init__(self, position):
        super().__init__(position, getPath('assets', 'Zombie-'))
        self._nextMovements = []

    def update(self, mapInfo):
        if not self._nextMovements:
            self._nextMovements = PathFinding.backtrack_distance_grid(mapInfo.player_distance_grid, self.position)[1:4]
            self._update_direction()
        elif mapInfo.player.direction != MovementDirection.NoDirection:
            self._move(mapInfo, self._nextMovements[0])
            self._nextMovements.pop(0)
        else:
            return

        self._update_direction()

    def _update_direction(self):
        if self._nextMovements and self.is_movement_possible:
            next_pos = self._nextMovements[0]
            try:
                self.direction = MovementDirection((next_pos[0] - self.position[0], next_pos[1] - self.position[1]))
            except:
                self.direction = MovementDirection.NoDirection
                
            self.update_image_path()

    def is_movement_allowed(self, new_position, mapInfo):
        if super().is_movement_allowed(new_position, mapInfo):
            entity = mapInfo.get_entity_from_position(new_position)
            return isinstance(entity, Joueur) or entity is None 
        
        return False


class Mur(VisualEntity):
    def __init__(self, position):
        super().__init__(position, getPath('assets', 'mur'))

class Plancher(VisualEntity):
    def __init__(self, position):
        super().__init__(position, getPath('assets', 'texture_pierre'), False, -1)