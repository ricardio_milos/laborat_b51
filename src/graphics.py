import os
import pygame

class Graphic:
    _graphics = {}

    @staticmethod
    def updateGraphics(current_graphic_tick, max_graphic_tick_until_logic_tick):
        for priority, graphics in Graphic._graphics.items():
            for graphic in graphics:
                if graphic._nb_sprites == 0:
                    continue

                if current_graphic_tick % (max_graphic_tick_until_logic_tick / graphic._nb_sprites) == 0:
                    graphic.next_sprite()

    @staticmethod
    def __append_graphics(graphic):
        if not graphic.priority in Graphic._graphics:
            Graphic._graphics[graphic.priority] = []
        Graphic._graphics[graphic.priority].append(graphic)

    def __init__(self, image_path, priority=0):
        self.priority = priority
        self.image_path = None
        self._image_filename = None
        self._nb_sprites = 0
        self._current_sprite = 0
        self._images = []
        self._images_path = []
        self.set_image_path(image_path)
        
        Graphic.__append_graphics(self)

    def set_image_path(self, image_path):
        self.image_path = image_path
        if self.image_path is not None:
            sprite_index = 0
            while os.path.exists(self.image_path + str(sprite_index + 1) + ".png"):
                sprite_index += 1
            
            self._nb_sprites = sprite_index
            self._current_sprite = 0
            self._load_images()


    def next_sprite(self):
        self._current_sprite = (self._current_sprite + 1) % self._nb_sprites

    def _load_images(self):
        self._images.clear()
        if self._nb_sprites == 0:
            self._images.append(pygame.image.load(self.image_path + ".png"))
        else:
            for i in range(1, self._nb_sprites + 1):
                image = self.image_path + str(i) + ".png"
                self._images_path.append(image)
                self._images.append(pygame.image.load(image))
    
