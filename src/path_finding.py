from visual_entity import MoveableEntity

class Node():
    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.position == other.position
        else:
            return self.position == other

    def __hash__(self):
        return self.position.__hash__()

class PathFinding:

    @staticmethod
    def generate_distance_grid(mapInfo, start):
        distance_grid = {}
        starting_node = Node(None, start)
        distance = 0

        open_list = set()
        open_list.add(starting_node)

        neighbours = [ [-1,0], [1,0], [0,-1], [0,1] ]
        while open_list:
            for node in open_list:
                distance_grid[node.position] = (node, distance)

            nodes = open_list.copy()
            open_list.clear()
            distance += 1
            for node in nodes:
                for neighbour in neighbours:
                    neighbour_pos = (node.position[0] + neighbour[0], node.position[1] + neighbour[1])
                    inside = mapInfo.is_position_inside_map(neighbour_pos)
                    entity = mapInfo.get_entity_from_position(neighbour_pos)
                    tile_free = isinstance(entity, MoveableEntity) or entity is None
                    not_already_calculated = neighbour_pos not in distance_grid
                    if inside and tile_free and not_already_calculated:
                        open_list.add(Node(node, neighbour_pos))

        return distance_grid

    @staticmethod
    def backtrack_distance_grid(distance_grid, start):
        path = []
        if start in distance_grid:
            node = distance_grid[start][0]
            while node is not None:
                path.append(node.position)
                node = node.parent

        return path