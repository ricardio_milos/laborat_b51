#!/usr/bin/env python

import pygame
from app_enum import MovementDirection

class KeyHandler:
    _touches_enregistrees = {}

    @staticmethod
    def verifier_touche_pressee(dict_touche_pressee):
        for touche in KeyHandler._touches_enregistrees:
            if dict_touche_pressee[touche]:
                for handler in KeyHandler._touches_enregistrees[touche]:
                    handler.action_touche(touche)

    def __init__(self, touches):
        for touche in set(touches):
            if touche not in KeyHandler._touches_enregistrees:
                KeyHandler._touches_enregistrees[touche] = []
            KeyHandler._touches_enregistrees[touche].append(self)

    def action_touche(self, touche):
        pass


class PlayerHandler(KeyHandler):
    def __init__(self, player):
        super().__init__([pygame.K_w, pygame.K_a, pygame.K_s, pygame.K_d,
                          pygame.K_UP, pygame.K_LEFT, pygame.K_DOWN, pygame.K_RIGHT])
        self._player = player

    def action_touche(self, touche):
        if touche == pygame.K_UP or touche == pygame.K_w:  # Vers le haut
            self._player.direction = MovementDirection.North
        elif touche == pygame.K_LEFT or touche == pygame.K_a:  # Vers la gauche
            self._player.direction = MovementDirection.West
        elif touche == pygame.K_DOWN or touche == pygame.K_s:  # Vers le bas
            self._player.direction = MovementDirection.South
        elif touche == pygame.K_RIGHT or touche == pygame.K_d:  # Vers la droite
            self._player.direction = MovementDirection.East
        else:
            return

        self._player.update_image_path()

class ExitHandler(KeyHandler):
    def __init__(self, app):
        KeyHandler.__init__(self, [pygame.K_ESCAPE])
        self._app = app

    def action_touche(self, touche):
        self._app.requestQuit()

class RestartHandler(KeyHandler):
    def __init__(self, app):
        KeyHandler.__init__(self, [pygame.K_SPACE])
        self._app = app

    def action_touche(self, touche):
        self._app.requestRestart()