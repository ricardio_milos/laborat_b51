#!/usr/bin/env python

from laborat_app import LaboratApp

def main():
    app = LaboratApp.getInstance()
    app.executable()


if __name__ == "__main__":
    main()
