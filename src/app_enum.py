import enum

class AppState(enum.Enum):
    Stopped = 0
    Running = 1
    Quit = 2
    Victory = 3
    Defeat = 4

class MovementDirection(enum.Enum):
    North = 0, -1
    West = -1, 0
    South = 0, 1
    East = 1, 0
    NoDirection = 0, 0